import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { FaCheck, FaTrashO, FaBan } from 'react-icons/lib/fa';

import { fetchBoardList, selectBoard, finishBoardCreation, deleteBoard, editBoard } from '../actions/boardList';

class BoardItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editMode: this.props.editMode,
      title: this.props.title,
      createMode: this.props.createMode
    }

    this.changeMode = this.changeMode.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  changeMode() {
    this.setState({ editMode: !this.state.editMode });
  }

  handleChange(event) {
    event.preventDefault();
    this.setState({title: event.target.value});
  }

  handleSubmit(event) {
    const data = {
      title: this.state.title,
      color: '#FAFAFA',
      period: 'weekly',
      visualization: 'kanban'
    };

    if (this.state.createMode) {
      this.props.finishBoardCreation(data);
      this.setState({createMode: false});
    }
    else {
      data['id'] = this.props.id;
      this.props.editBoard(data);
    }

    this.changeMode();
  }

  handleDelete(){
    this.props.deleteBoard(this.props.board.id)
  }

  componentDidMount() {
    if (this.state.createMode)
      this.changeMode();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.editMode !== nextProps.editMode){
      this.setState({editMode: nextProps.editMode});
    }
  }

  render() {
    const boardItemNormal = (
      <div 
        key={ this.props.board.id } 
        className="board-list-link"
        onClick={() => this.props.selectBoard(this.props.board)}>
       
        <Link 
          to={{ pathname: `/board/${ this.props.board.id }`,
          state: {board: this.props.board}}} 
          style={{ textDecoration: 'none', color: '#4f4f4f' }}
          params={{ id: this.props.board.id }}> 
          <div className="board-list-item">
            { this.state.title } 
          </div>
        </Link>
      </div>
    );

    const createIcons = (
      <div className="board-item-icons">
        <div onClick={this.handleSubmit}>
          <FaCheck />
        </div>
        <div onClick={this.changeMode}>
          <FaBan/>
        </div>
      </div>
    );

    const editIcons = (
      <div className="board-item-icons">
        <div onClick={this.handleSubmit}>
          <FaCheck />
        </div>
        <div onClick={this.handleDelete}>
          <FaTrashO />
        </div>
      </div>
    );

    const icons = this.state.createMode ? createIcons : editIcons

    const boardItemEdit = (
      <div key={ this.props.board.id } className="board-list-edit">
        <input 
          className="board-list-edit-item" 
          value={ this.state.title }
          onChange={this.handleChange}
          type="text"
        />
        { icons }
      </div>
    );

    const boardItem = this.state.editMode? boardItemEdit : boardItemNormal;

    return (boardItem);
  }
}

function mapDispatchToProps(dispatch) {
  const actions = {
    fetchBoardList: fetchBoardList,
    selectBoard: selectBoard,
    finishBoardCreation: finishBoardCreation,
    deleteBoard: deleteBoard,
    editBoard: editBoard
  }

  return bindActionCreators(actions, dispatch);
}

function mapStateToProps(state) {
  return {
    activeCards: state.activeCards
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BoardItem);
