import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FaCheck, FaBan, FaPlus, FaTrashO } from 'react-icons/lib/fa';

import { fetchTaggings, finishCardCreation, deleteCard, editCard, addTagging, deleteTagging } from '../actions/activeCards';

class Card extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editMode: false,
      createMode: this.props.createMode,
      title: this.props.title,
      date: this.props.date,
      priority: this.props.priority,
      listId: this.props.listId,
      tagAdd: false,
      newTag: null
    };

    this.changeMode = this.changeMode.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.changeTagMode = this.changeTagMode.bind(this);
  }

  componentDidMount() {
    if (this.state.createMode)
      this.changeMode();
    else
      this.props.fetchTaggings(this.props.id);
  }

  renderTag(cardId, taggings) {
    return taggings[cardId].map(id => {
      const tag = this.props.activeTags[id];

      if (tag === undefined)
        return null;

      const tag_jsx = (
        <div key={`${tag.id}_tag_${cardId}`} className="card-tag">
          { tag.title }
        </div>
      )

      const tag_jsx_edit = (
        <div className="card-tag-edit">
          <div>
            { tag.title }
          </div>
          <div onClick={(e) => this.deleteTagging(tag.id, cardId, e)}>
            <FaTrashO />
          </div>
        </div>        
      );
      
      if (this.state.editMode)
        return tag_jsx_edit;
      else
        return tag_jsx;
    });
  }

  renderTags(cardId, taggings) {
    if (taggings === [] || taggings[cardId] === undefined)
      return;
  
    const tags = this.props.activeTags
    const tagsKeys = Object.keys(tags)
    const tagAdd = (
      <div>
        <div>
          {this.renderTag(cardId, taggings)}
        </div>
        <select className="card-tag" id="myTag">
          {
            tagsKeys.map(key => {
              return(
                <option value={ tags[key].id }> { tags[key].title } </option>
              );                
            })
          }
        </select>
        <div onClick={(e) => this.addTagging(document.getElementById("myTag").value, cardId, e)}>
          <FaCheck/>
        </div>
      </div>
    );

    const tagRegular = (
      this.renderTag(cardId, taggings)
    );

    const finalTag = this.state.tagAdd ? tagAdd : tagRegular

    return(
      finalTag 
    );
  }

  deleteTag(id) {}

  changeMode() {
    this.setState({ editMode: !this.state.editMode });
  }

  handleChange(event) {
    let obj = {}
    const regex = /^card-(.+)-edit/g
    const key = regex.exec(event.target.className)[1];
    obj[key] = event.target.value;
    this.setState(obj);
  }

  handleSubmit() {
    let card = {
      title:    this.state.title,
      priority: this.state.priority,
      date:     this.state.date
    }

    if (this.state.createMode) {
      this.props.finishCardCreation(card, this.props.listId);
      this.setState({createMode : false});
    }

    else
      this.props.editCard(this.props.id, card);

    this.changeMode();
  }

  handleDelete(event) {
    this.props.deleteCard(this.props.listId, this.props.id);
  }

  changeTagMode(){
    this.setState({tagAdd: !this.state.tagAdd})
  }

  addTagging(tagId, cardId, event){
    this.props.addTagging(cardId, tagId);
    this.changeTagMode();
  }

  deleteTagging(tagId, cardId, event){
    this.props.deleteTagging(cardId, tagId);
  }

  render() {
    const { id } = this.props;

    const addTag = (
      <div className="card-add-tag-button"
            onClick={this.changeTagMode}>
        <FaPlus/>
      </div>
    );
    
    const checkTag = (
      <div className="card-add-tag-button"
            onClick={this.changeTagMode}>
        <FaBan/>
      </div>
    );

    const tagIcon = this.state.tagAdd ? checkTag : addTag;

    const editCard = (
      <div className="card card-grid-edit" key={id + '_card'}>
        <div className="card-tag-list">
          { this.renderTags(id, this.props.activeTaggings) }
        </div>

        { tagIcon }

        <input
          type="text"
          className="card-title-edit card-input"
          value = {this.state.title}
          onChange = {this.handleChange}
        />

        <input
          type="text"
          className="card-date-edit"
          value = {this.state.date}
          onChange = {this.handleChange}
        />

        <input
          type="text"
          className="card-priority-edit"
          value = {this.state.priority}
          onChange = {this.handleChange}
        />


        <div className="card-button-save" onClick={this.handleSubmit}>
          <FaCheck/>
        </div>

        <div className="card-button-cancel" onClick={this.changeMode}>
          <FaBan/>
        </div>

        <div className="card-button-delete" onClick={this.handleDelete}>
          <FaTrashO/>
        </div>
      </div>
    );

    const card = (
      <div className="card card-grid" key={id + '_card'}
           onClick={ this.changeMode.bind(this) }>
        <div className="card-tag-list">
          { this.renderTags(id, this.props.activeTaggings) }
        </div>

        <div className="card-title">
          { this.state.title }
        </div>

        <div className="card-priority">
          { this.state.priority }
        </div>

        <div className="card-date">
          { this.state.date }
        </div>
      </div>
    );

  const renderedCard = this.state.editMode ? editCard : card;

  return(renderedCard);
  }
}

function mapDispatchToProps(dispatch) {
  const actions = {
    fetchTaggings: fetchTaggings,
    finishCardCreation: finishCardCreation,
    editCard: editCard,
    deleteCard: deleteCard,
    addTagging: addTagging,
    deleteTagging: deleteTagging
  }

  return bindActionCreators(actions, dispatch);
}

function mapStateToProps(state) {
  return {
    activeCards: state.activeCards,
    activeTags: state.activeTags,
    activeTaggings: state.activeTaggings
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Card);
