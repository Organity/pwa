import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FaPlus, FaPencil, FaCheck, FaTrashO } from 'react-icons/lib/fa';

import Card from './activeCard'

import { fetchCards, createCard, finishListCreation, deleteList, editList } from '../actions/activeList'

class List extends Component {

  constructor(props) {
    super(props);

    this.state = {
      editMode: false,
      title: this.props.title,
      createMode: this.props.createMode
    }

    this.changeMode = this.changeMode.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
  }

  renderCards(cards) {
    if (cards === [] || cards[this.props.id] === undefined)
      return;

    return cards[this.props.id].map(({ id, title, priority, color, date}) => {
      let createMode = (id === 'new');
      return (
        <Card
          key = { `${id}_card` }
          id = { id }
          title = { title }
          priority = { priority }
          color = { color }
          date = { date }
          createMode = { createMode }
          listId = { this.props.id }
        />
      )
    });
  }

  changeMode() {
    this.setState({ editMode: !this.state.editMode });
  }

  handleCreate() {
    this.props.createCard(this.props.id);
  }

  handleChange(event) {
    event.preventDefault();
    this.setState({title: event.target.value});
  }

  handleSubmit(event) {
    let list = {
      title: this.state.title,
      color: this.props.color,
      sort: this.props.sort,
      board_id: this.props.boardId
    }

    if (this.state.createMode) {
      this.props.finishListCreation(list);
      this.setState({createMode: false});
    }

    else {
      list['id'] = this.props.id;
      this.props.editList(list)
    }

    this.changeMode();
  }

  handleDelete(event) {
    this.props.deleteList(this.props.id);
  }

  componentDidMount() {
    if (this.state.createMode)
      this.changeMode();
    else
      this.props.fetchCards(this.props.id);
  }

  render() {
    const titleNormal = (
      <div className="list-title">
        <div className="list-title-text"> { this.state.title } </div>
        <div className="list-title-icon list-title-icon-right" onClick={this.changeMode}>
          <FaPencil/>
        </div>
        <div className="list-title-icon"  onClick={this.handleDelete}>
          <FaTrashO/>
        </div>
      </div>
    );

    const titleEdit = (
      <div className="list-title">
        <input className="list-title-text list-title-input" value={this.state.title} onChange={this.handleChange}/>
        <FaCheck className="list-title-icon" onClick={this.handleSubmit}/>
      </div>
    );

    const title = this.state.editMode? titleEdit : titleNormal;

    return (
      <div className="list">
        <div className="list-start list-centralized">
          { title }

          <div className="list-cards">
            { this.renderCards(this.props.activeCards) }
          </div>
        </div>

        <div className="list-end">
          <div className="list-add-card-button" onClick={this.handleCreate}>
            <FaPlus className="list-add-card-button-icon"/>
            <div className="list-add-card-button-text"> Adicionar Card </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  const actions = {
    fetchCards: fetchCards,
    createCard: createCard,
    finishListCreation: finishListCreation,
    editList: editList,
    deleteList: deleteList
  }

  return bindActionCreators(actions, dispatch);
}

function mapStateToProps(state) {
  return {
    activeCards: state.activeCards
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(List);
