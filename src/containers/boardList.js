import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FaPlus, FaPencil } from 'react-icons/lib/fa';

import { fetchBoardList, selectBoard, createBoard, finishBoardCreation } from '../actions/boardList';

import BoardItem from './boardItem';

class BoardList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editMode: false
    }

    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.changeMode = this.changeMode.bind(this);
  }

  componentDidMount() {
    this.props.fetchBoardList();
  }

  renderBoards(boards) {
    return boards.map((board) => {
      let createMode = (board.id === 'new');
      return(
        <BoardItem 
          key={board.id}
          id={board.id}
          board={board}
          title={board.title}
          createMode={createMode}
          editMode={this.state.editMode}
        /> 
      );
    });
  }

  handleButtonClick() {
    this.props.createBoard(this.props.match.params.id);
  }

  changeMode() {
    this.setState({editMode: !this.state.editMode},
                   () => {this.renderBoards(this.props.boardList)})
  }

  render() {
    return (
      <div className="board-list-wrapper">
        <div className="board-list-header">
          <div className="board-list-header-title">
            Organity
          </div>
          <div className="board-list-menu">
            <div className="board-list-menu-item"
                onClick={this.handleButtonClick}>
              <FaPlus/>
              <p> Novo Board</p>
            </div>
            <div className="board-list-menu-item"
                 onClick={this.changeMode}>
              <FaPencil />
              <p> Editar Boards</p>
            </div>
          </div>
        </div>

        <div className="board-list-list">
          { this.renderBoards(this.props.boardList) }
        </div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  const actions = {
    fetchBoardList: fetchBoardList,
    selectBoard: selectBoard,
    createBoard: createBoard,
    finishBoardCreation: finishBoardCreation
  }

  return bindActionCreators(actions, dispatch);
}

function mapStateToProps(state) {
    return {
      boardList: state.boardList
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BoardList);
