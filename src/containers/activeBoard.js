import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FaPlus } from 'react-icons/lib/fa';

import List from './activeList';
import { fetchLists, fetchTags, createList } from '../actions/activeBoard';


class Board extends Component {
  constructor(props) {
    super(props);

    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  renderLists(lists) {
    return lists.map(({ id, title, color, sort }) => {
      let createMode = (id === 'new');
      return (
        <List
          key={ `${id}_list` }
          id={ id }
          title={ title }
          color={ color }
          sort={ sort }
          boardId={ this.props.match.params.id }
          createMode = { createMode }
        />
      );
    });
  }

  componentDidMount() {
    this.props.fetchLists(this.props.match.params.id);
    this.props.fetchTags(this.props.match.params.id);
  }

  handleButtonClick() {
    this.props.createList(this.props.match.params.id);
  }

  render() {
    return (
      <div className="board">
        <div className="board-header">
          <div className="board-header-title">
            { this.props.location.state.board.title }
          </div>

          <div className="board-header-button" onClick={this.handleButtonClick}>
            <FaPlus className="board-header-button-icon"/>
            <div className="board-header-button-text"> Nova Lista </div>
          </div>
        </div>

        <div className="board-lists">
         { this.renderLists(this.props.activeLists) }
        </div>
      </div>
    );

  }
}

function mapDispatchToProps(dispatch) {
  const actions = {
    fetchLists: fetchLists,
    fetchTags: fetchTags,
    createList: createList
  }

  return bindActionCreators(actions, dispatch);
}

function mapStateToProps(state) {
  return {
    activeLists: state.activeLists,
    activeTags: state.activeTags
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Board);
