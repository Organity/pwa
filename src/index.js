import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './components/app';
import store from './stores/configureStore';
import registerServiceWorker from './registerServiceWorker';

import './styles/index.css';

export const baseAPIUrl = (process.env.NODE_ENV === "development") ?
  'http://localhost:3000' : 'https://organity-api.herokuapp.com';

ReactDOM.render(
  <Provider store={ store }>
    <App />
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();
