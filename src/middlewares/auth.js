import axios from 'axios';

export const updateToken = (response) => {
  const { 
    'access-token': token,
    'token-type': tokenType,
    client,
    uid, 
    expiry
  } = response.headers;

  localStorage.setItem('access-token', token);
  localStorage.setItem('client', client);
  localStorage.setItem('token-type', tokenType);
  localStorage.setItem('uid', uid);
  localStorage.setItem('expiry', expiry);
  localStorage.setItem('userId', response.data.id);

}

export const prepareAxios = () => {
  const headers = ['access-token', 'token-type', 'client', 'uid', 'expiry'];

  headers.forEach(token => 
    axios.defaults.headers.common[token] = localStorage.getItem(token)
  );
}