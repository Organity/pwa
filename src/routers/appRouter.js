import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import BoardList from '../containers/boardList';
import Board from '../containers/activeBoard';
import LoginForm from '../components/login';
import SignUpForm from '../components/signup';

export default () => {
  return (
    <BrowserRouter>
      <div>
        <Switch>
          <Route path='/signup'    component={ SignUpForm } exact={ true }/>
          <Route path='/'     component={ LoginForm }  exact={ true }/>
          <Route path='/board'     component={ BoardList }  exact={ true }/>
          <Route path='/board/:id' component={ Board } />
          </Switch>
      </div>
    </BrowserRouter>
  );
};
