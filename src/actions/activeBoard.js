import axios from 'axios';

import { baseAPIUrl } from '../index';
import { prepareAxios } from '../middlewares/auth';

export const FETCH_LISTS = 'FETCH_LISTS';
export const FETCH_TAGS = 'FETCH_TAGS';
export const CREATE_LIST = 'CREATE_LIST';

export const fetchLists = board_id => async dispatch => {
  prepareAxios();
  const response = await axios.get(`${baseAPIUrl}/boards/${board_id}/lists`);

  dispatch({
    type: FETCH_LISTS,
    activeLists: response.data
  });
}

export const fetchTags = board_id => async dispatch => {
  prepareAxios();
  const response = await axios.get(`${baseAPIUrl}/boards/${board_id}/tags`);

  dispatch({
    type: FETCH_TAGS,
    activeTags: response.data
  })
}

export const createList = boardId => {
  return {
    type: CREATE_LIST,
    boardId,
    list: {
      id: 'new',
      title: 'nova lista',
      color: '#FFFFFF',
      sort: 'priority',
      board_id: boardId
    }
  }
}

