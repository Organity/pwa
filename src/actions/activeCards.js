import axios from 'axios';

import { baseAPIUrl } from '../index';
import { prepareAxios } from '../middlewares/auth';

export const FETCH_TAGGINGS= 'FETCH_TAGGINGS';
export const FINISH_CARD_CREATION = 'FINISH_CARD_CREATION';
export const DELETE_CARD = 'DELETE_CARD';

export const ADD_TAGGING = 'ADD_TAGGING';
export const DELETE_TAGGING = 'DELETE_TAGGING';

export const fetchTaggings = cardId => async dispatch => {
  prepareAxios();
  const response = await axios.get(`${baseAPIUrl}/cards/${cardId}/tags`)

  const activeTaggings = response.data.map(tag => {
    return tag.id
  });

  dispatch({
    type: FETCH_TAGGINGS,
    activeTaggings,
    cardId
  })
}

export const finishCardCreation = (card, listId) => async dispatch => {
  card['color'] = '#FFFFFF';

  prepareAxios();
  const response = await axios.post(`${baseAPIUrl}/cards`, card);

  await axios.post(`${baseAPIUrl}/listings/`, {
    list_id: listId,
    card_id: response.data.id
  });

  dispatch({
    type: FINISH_CARD_CREATION,
    listId,
    card: response.data
  });
}

export const editCard = (cardId, card) => async dispatch => {
  prepareAxios();
  await axios.put(`${baseAPIUrl}/cards/${cardId}`, card);
}

export const deleteCard = (listId, cardId) => async dispatch => {
  if (cardId === undefined) return;

  prepareAxios();
  await axios.delete(`${baseAPIUrl}/cards/${cardId}`);

  dispatch({
    type: DELETE_CARD,
    listId,
    cardId
  });
}

export const addTagging = (cardId, tagId) => async dispatch => {
  prepareAxios();
  await axios.post(`${baseAPIUrl}/taggings`, {
    card_id: cardId,
    tag_id: tagId
  });

  dispatch({
    type: ADD_TAGGING,
    cardId,
    tagId
  })
}

export const deleteTagging = (cardId, tagId) => async dispatch => {
  prepareAxios();

  const response = await axios.get(`${baseAPIUrl}/taggings`);
  let deletedTagging;

  for (let tagging of response.data) {
    if (tagging.card_id === cardId && tagging.tag_id === tagId) {
      deletedTagging = tagging;
      break;
    }
  }

  await axios.delete(`${baseAPIUrl}/taggings/${deletedTagging.id}`);

  dispatch({
    type: DELETE_TAGGING,
    cardId,
    tagId
  })
}