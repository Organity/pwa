import axios from 'axios'

import { baseAPIUrl } from '../index'
import { prepareAxios } from '../middlewares/auth';

export const CREATE_CARD = 'CREATE_CARD';
export const FETCH_CARDS = 'FETCH_CARDS';
export const FINISH_LIST = 'FETCH_CARDS';
export const DELETE_LIST = 'DELETE_LIST';
export const FINISH_LIST_CREATION = 'FINISH_LIST_CREATION';

export const fetchCards = listId => async dispatch => {
  prepareAxios();
  const response = await axios.get(`${baseAPIUrl}/lists/${listId}/cards`);

  dispatch({
    type: FETCH_CARDS,
    activeCards: response.data,
    listId
  });
}

export const createCard = listId => {
  return {
    type: CREATE_CARD,
    listId,
    card: {
      id: 'new',
      title: 'novo card',
      color: '#FFFFFF',
      priority: 0,
      date: 'YYYY/MM/DD 00:00'
    }
  }
}

export const finishListCreation = list => async dispatch => {
  prepareAxios();
  const response = await axios.post(`${baseAPIUrl}/lists`, list);

  dispatch({
    type: FINISH_LIST_CREATION,
    list: response.data
  });
}

export const editList = list => async dispatch => {
  prepareAxios();
  await axios.put(`${baseAPIUrl}/lists/${list.id}`, list);
}

export const deleteList = listId => async dispatch => {
  prepareAxios();
  await axios.delete(`${baseAPIUrl}/lists/${listId}`);

  dispatch({
    type: DELETE_LIST,
    listId
  });
}
