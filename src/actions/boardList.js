import axios from 'axios';

import { baseAPIUrl } from '../index';
import { prepareAxios } from '../middlewares/auth';

export const FETCH_BOARDS = 'FETCH_BOARDS';
export const SELECT_BOARD = 'CHANGE_ACTIVE_BOARD';
export const CREATE_BOARD = 'CREATE_BOARD';
export const FINISH_BOARD_CREATION = 'FINISH_BOARD_CREATION';
export const DELETE_BOARD = 'DELETE_BOARD';

export const fetchBoardList = () => async dispatch => {
  prepareAxios();
  const response = await axios.get( `${baseAPIUrl}/boards`);

  dispatch({
    type: FETCH_BOARDS,
    boardList: response.data
  });
}

export const selectBoard = board => {
  return {
    type: SELECT_BOARD,
    payload: board
  }
}

export const createBoard = userId => {
  return {
    type: CREATE_BOARD,
    userId,
    board: {
      id: 'new',
      title: 'Nova Board',
      color: '#FFFFFF',
      period: 'weekly',
      visualization: 'kanban'
    }
  }
}

export const finishBoardCreation = (board, userId) => async dispatch => {
  prepareAxios();
  const response = await axios.post(`${baseAPIUrl}/boards`, board);

  await axios.post(`${baseAPIUrl}/creations`, {
    user_id: 1,
    board_id: response.data.id
  });

  dispatch({
    type: FINISH_BOARD_CREATION,
    board: response.data
  })
}

export const editBoard = board => async dispatch => {
  console.log(board);
  prepareAxios();
  await axios.put(`${baseAPIUrl}/boards/${board.id}`, board);
}

export const deleteBoard = boardId => async dispatch => {
  prepareAxios();
  await axios.delete(`${baseAPIUrl}/boards/${boardId}`);

  dispatch({
    type: DELETE_BOARD,
    boardId
  })
}
