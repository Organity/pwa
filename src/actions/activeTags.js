import axios from 'axios';
import { baseAPIUrl } from '..';
import { prepareAxios } from '../middlewares/auth';

export const EDIT_TAG = 'EDIT_TAG';
export const CREATE_TAG = 'CREATE_TAG';
export const FINISH_TAG_CREATION = 'FINISH_TAG_CREATION';
export const DELETE_TAG = 'DELETE_TAG';

export const createTag = boardId => {
  return {
    type: CREATE_TAG,
    tag: {
      id: 'new',
      title: 'nova tag',
      color: '#ffffff',
      board_id: boardId
    }
  }
}

export const finishTagCreation = tag => async dispatch => {
  prepareAxios();
  const response = await axios.post(`${baseAPIUrl}/tags`, tag);
  
  dispatch({
    type: FINISH_TAG_CREATION,
    tag: response.data
  })
}

export const deleteTag = tagId => async dispatch => {
  prepareAxios();
  await axios.delete(`${baseAPIUrl}/tags/${tagId}`);

  dispatch({
    type: DELETE_TAG,
    tagId
  })
}

export const editTag = tag => async dispatch => {
  prepareAxios();
  await axios.put(`${baseAPIUrl}/tags/${tag.id}`, tag);
}