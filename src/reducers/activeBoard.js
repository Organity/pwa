import { SELECT_BOARD } from '../actions/boardList'
import { initialState } from '../stores/configureStore';

export default (state = initialState, action) => {
  switch(action.type) {
    case SELECT_BOARD:
      return action.payload;

    default:
      return state;
  }
}
