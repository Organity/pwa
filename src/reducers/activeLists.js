import { FETCH_LISTS, CREATE_LIST } from '../actions/activeBoard';
import { FINISH_LIST_CREATION } from '../actions/activeList';
import { DELETE_LIST } from '../actions/activeList';
import { initialState } from '../stores/configureStore';

export default (state = initialState, action) => {
  switch(action.type) {
    case FETCH_LISTS:
      return action.activeLists;

    case CREATE_LIST:
      return [ ...state, action.list ];

    case FINISH_LIST_CREATION:
      let newLists = [];

      for (let list of state)
        if (list.id === 'new')
          newLists.push(action.list)
        else
          newLists.push(list)

      return newLists;

    case DELETE_LIST:
      let nonDeletedLists = [];

      for (let list of state)
        if (list.id !== action.listId)
          nonDeletedLists.push(list);

      return nonDeletedLists;

    default:
      return state;
  }
}
