import { initialState } from '../stores/configureStore';
import { FETCH_CARDS, CREATE_CARD } from '../actions/activeList';
import { FINISH_CARD_CREATION, DELETE_CARD } from '../actions/activeCards';

export default (state = initialState, action) => {
  let obj = {};

  switch(action.type) {
    case FETCH_CARDS:
      obj[action.listId] = action.activeCards;

      return Object.assign({}, state, obj);

    case CREATE_CARD:
      obj[action.listId] = [ action.card, ...state[action.listId] ];

      return Object.assign({}, state, obj);

    case FINISH_CARD_CREATION:
      const newCards = [];

      for (let card of state[action.listId])
        if (card.id === 'new')
          newCards.push(action.card);
        else
          newCards.push(card);

      obj[action.listId] = newCards;
      return Object.assign({}, state, obj);

    case DELETE_CARD:
      let nonDeletedCards = [];

      for (let card of state[action.listId])
        if (card.id !== action.cardId)
          nonDeletedCards.push(card);

      obj[action.listId] = nonDeletedCards;
      return Object.assign({}, state, obj);

    default:
      return state;
  }
}
