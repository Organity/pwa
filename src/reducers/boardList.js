import { FETCH_BOARDS, CREATE_BOARD, FINISH_BOARD_CREATION, DELETE_BOARD } from '../actions/boardList';
import { initialState } from '../stores/configureStore';

export default (state = initialState, action) => {
  switch(action.type) {

    case FETCH_BOARDS:
      return action.boardList

    case CREATE_BOARD:
      return [ ...state, action.board ];

    case FINISH_BOARD_CREATION:
      let newBoards = [];

      for (let board of state)
        if (board.id === 'new')
          newBoards.push(action.board);
        else
          newBoards.push(board);
      
      return newBoards;

    case DELETE_BOARD:
      let nonDeletedBoards = [];

      for (let board of state)
        if (board.id !== action.boardId)
          nonDeletedBoards.push(board);

      return nonDeletedBoards;
    default:
      return state;
  }
}
