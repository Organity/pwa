import { initialState } from '../stores/configureStore';
import { FETCH_TAGGINGS, ADD_TAGGING, DELETE_TAGGING } from '../actions/activeCards';
import { DELETE_TAG } from '../actions/activeTags';

export default (state = initialState, action) => {
  switch(action.type) {
    case FETCH_TAGGINGS:
      let obj = {};
      obj[action.cardId] = action.activeTaggings;

      return Object.assign({}, state, obj);

    case ADD_TAGGING:
      const newTaggings = [ ...state[action.cardId], action.tagId ];
      obj[action.cardId] = newTaggings;

      return Object.assign({}, state, obj);

    case DELETE_TAGGING:
      let nonDeletedTaggings = [];

      for (let tagging of state[action.cardId])
        if (tagging.tagId !== action.tagId)
          nonDeletedTaggings.push(tagging);

      obj[action.cardId] = nonDeletedTaggings;
      return Object.assign({}, state, obj);
    
    case DELETE_TAG:
      let newState = [];

      for (let card of state) {
        let taggings = [];
        
        for (let tag of card)
          if (tag.id !== action.tagId)
            taggings.push(tag);
        
        newState.push(taggings);
      }

      return newState;


    default:
      return state;
  }
}
