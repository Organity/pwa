import { FETCH_TAGS } from '../actions/activeBoard';
import { initialState } from '../stores/configureStore';
import { FINISH_TAG_CREATION, CREATE_TAG, DELETE_TAG } from '../actions/activeTags';

export default (state = initialState, action) => {
  switch(action.type) {
    case FETCH_TAGS:
      let obj = {};

      for (let tag of action.activeTags)
        obj[tag.id] = tag;

      return Object.assign({}, state, obj);

    case CREATE_TAG:
      return [ ...state, action.tag ];

    case FINISH_TAG_CREATION:
      let newTags = [];

      for (let tag of state)
        if (tag.id === 'new')
          newTags.push(action.tag);
        else
          newTags.push(tag);

      return newTags;

    case DELETE_TAG:
      let nonDeletedTags = [];
    
      for (let tag of state)
        if (tag.id !== action.tagId)
          nonDeletedTags.push(tag);

      return nonDeletedTags;

    default:
      return state;
  }
}
