import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import ReduxThunk from 'redux-thunk';

import BoardsListReducer     from '../reducers/boardList';
import ActiveBoardReducer    from '../reducers/activeBoard';
import ActiveListsReducer    from '../reducers/activeLists';
import ActiveTagsReducer     from '../reducers/activeTags';
import ActiveCardsReducer    from '../reducers/activeCards';
import ActiveTaggingsReducer from '../reducers/activeTaggings';

export const initialState = {
  boardList:   [],
  activeBoard: null,
  activeLists: [],
  activeTags:  [],
  activeCards: [],
  activeTaggings: []
}

const rootReducer = combineReducers({
  boardList:   BoardsListReducer,
  activeBoard: ActiveBoardReducer,
  activeLists: ActiveListsReducer,
  activeTags:  ActiveTagsReducer,
  activeCards: ActiveCardsReducer,
  activeTaggings: ActiveTaggingsReducer
})

export default createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(ReduxThunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);

