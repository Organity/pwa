import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

import { baseAPIUrl } from '../index';

export default class SignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      passwordConfirmation: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    event.preventDefault();

    let regex = /(.*)-input/g
    const key = regex.exec(event.target.className)[1];
    let data = {};
    data[key] = event.target.value;

    this.setState(data);
  }

  async handleSubmit(event) {
    event.preventDefault();

    const payload = {
      email: this.state.email,
      password: this.state.password,
      password_confirmation: this.state.passwordConfirmation
    }

    const new_state = {
      email: '',
      password: '',
      passwordConfirmation: ''
    }

    this.setState(new_state);

    await axios.post(`${baseAPIUrl}/auth`, payload);
    this.props.history.push('/');
  }

  render() {
    return (
      <div className="signup-wrapper">
        
        <div className="signup-header">
          <div className="signup-title">
            Organity
          </div>
        </div>
      
        <form className="signup-form"
              onSubmit={this.handleSubmit}>
          <input
            type="text"
            className="email-input"
            value={this.state.email}
            onChange={this.handleChange}
            placeholder="email"
            type="email"
          />

          <input
            type="text"
            className="password-input"
            value={this.state.password}
            onChange={this.handleChange}
            placeholder="password"
            type="password"
          />

          <input
            type="text"
            className="passwordConfirmation-input"
            value={this.state.passwordConfirmation}
            onChange={this.handleChange}
            placeholder="password confirmation"
            type="password"
          />

          <button type="submit">
            Cadastrar
          </button>
          
          <Link to="/"
                className="signup-link"
                style={{ textDecoration: 'none',
                          color: '#4f4f4f'}}>
            Voltar
          </Link>
        </form>
      </div>
    );
  }
}
