import React, { Component } from 'react';
import axios from 'axios';

import { Link } from 'react-router-dom';
import { baseAPIUrl } from '../index';
import { updateToken } from '../middlewares/auth';

export default class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    event.preventDefault();

    let regex = /(.*)-input/g
    const key = regex.exec(event.target.className)[1];
    let data = {};
    data[key] = event.target.value;

    this.setState(data);
  }

  async handleSubmit(event) {
    event.preventDefault();

    const payload = {
      email: this.state.email,
      password: this.state.password,
    }

    const new_state = {
      email: '',
      password: '',
    }

    this.setState(new_state);

    const response = await axios.post(`${baseAPIUrl}/auth/sign_in`, payload);
    updateToken(response);

    this.props.history.push('/board');
  }

  render() {
    return (
      <div className="login-wrapper">
        <div className="login-header">
          <div className="login-title">
            Organity
          </div>
        </div>
        <div >
          <form className="login-form"
                onSubmit={this.handleSubmit}>
            
            <input
              type="text"
              className="email-input"
              value={this.state.email}
              onChange={this.handleChange}
              type="email"
              placeholder="email"
            />

            <input
              type="text"
              className="password-input"
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
              placeholder="password"
            />

            <button type="submit">Entrar</button>
            
            <Link className=" login-link"
                  style={{ textDecoration: "none",
                           color: "#4f4f4f"}}
                  to="/signup">Cadastrar-se</Link>
          </form>
        </div>
      </div>
    );
  }
}