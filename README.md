# README

## Requirements

This project uses Docker for development, test and deployment.

Install Docker by following the instructions in one of the following
links, depending on your OS:
- Mac: https://docs.docker.com/docker-for-mac/install/
- Windows: https://docs.docker.com/docker-for-windows/install/
- Ubuntu: https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/
- Debian: https://docs.docker.com/engine/installation/linux/docker-ce/debian/
- Arch Linux: https://wiki.archlinux.org/index.php/Docker

To make the developmente easier, the project alse uses Docker Compose
to orchestrate multiple containers together.

Install Docker Compose by following the instructions in
https://docs.docker.com/compose/install/.

## Setup

- To set up the API server, run:
  ```
  docker-compose build
  docker-compose up
  ```
  **NOTE:** These commands will **create a Docker image for the PWA**,
  and **load the server**. By default, the PWA will be available at
  `localhost:8080`.
  
## Observations

- The `build` operation may take a while depending on your internet connetion.
  It will download all dependencies specified at `package.json` and `yarn.lock`
  whenever these files are changed.

- The PWA assumes that the API is running at `localhost:3000`.